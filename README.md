# `tezos-indexer`, an indexer for Tezos

## What it's for

Say you want to see the last 10 transactions for an account.
If you use `tezos-client` to get the information, you'll have to query the chain's
blocks one by one, starting from the latest, until you find 10 of them.
If you wanted all transactions for that account, you'd have to browse the whole
chain.

The job of an "indexer" is to make such operations easy and fast, and how it does
that is by *indexing* those information, hence its name.

Indexers are the backbones of block explorers, dApps, and wallets.

## How to use it

This indexer connects to a `tezos-node`, queries blocks, and puts them in a database.
We're using PostGres to handle the database, a popular engine, so that you can 
access the data using virtually any programming language you want.

The scheme of the database is available in [chain.sql](https://gitlab.com/nomadic-labs/tezos-indexer/blob/mainnet/src/lib_indexer/chain.sql).

## Where to find it

The indexer for a given Tezos branch should be in a branch with the
same name here if it exists (except for branch `master`).

If you want to use the indexer for the Tezos `mainnet` network,
check the [branch `mainnet`](https://gitlab.com/nomadic-labs/tezos-indexer/blob/mainnet/).

If you want to use the indexer for the Tezos `alphanet` network,
check the [branch `alphanet`](https://gitlab.com/nomadic-labs/tezos-indexer/blob/alphanet/).

Other networks are not supported *yet* (or perhaps they are, if this has become out of date).

## Issues etc.

You're very welcome to [raise issues if you have any](https://gitlab.com/nomadic-labs/tezos-indexer/issues) and/or to [submit merge requests](https://gitlab.com/nomadic-labs/tezos-indexer/merge_requests).

## Development version

At time of writing, the `zeronet` branch doesn't exist, but the `zeronet-dev` does.
That means the contents of `zeronet-dev` is not ready to become `zeronet`.
You're welcome to try a branch that has the `-dev` suffix, but it might be in an unstable state.

Bug reports are welcome, as for merge requests.


## Contact

You're welcome to create issues or merge requests. 
You may also contact [Philippe Wang](@philippewang.info) by other means (email, slack, messenger, sms, telegram, linkedin, etc.) as long as it works.
You may also contact any member of Nomadic Labs but they'll quite likely redirect you to Philippe Wang.
And if you want your name added here, make a merge request! ;)

